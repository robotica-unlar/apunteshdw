# STM32 {#stm32}

Fabricados por [ST Microelectronics](https://www.st.com) son placas de desarrollo con procesadores ARM en lugar de los AVR típicos de Arduino.

Estas placas proveen potencia y características avanzadas a un precio realmente bajo (proporcionalmente), y es posible programarlas directamente desde el mismo IDE de Arduino.

Estas placas operan a 3.3V pero la mayoría de sus I/O digitales son tolerantes a 5V.


## STM32F103C8T6 Blue Pill

Llamada así por su característico color azul, lleva un procesador ARM Cortex M3 a 72 MHz

![](imagenes/STM32F103C8T6_Blue_Pill-4.jpg)
Fuente: [link](https://stm32-base.org/boards/STM32F103C8T6-Blue-Pill)

Especificaciones Técnicas:

* Board
  * Name	Blue Pill
  * Part	Unknown
  * Brand	Unknown
  * Origin	China
* Microcontroller
  * Part	STM32F103C8T6
  * Manufacturer	ST-Microelectronics
  * Core	Arm Cortex-M3
  * Max. Clock Speed	72MHz
  * Package	LQFP 48 pins
* Internal memories
  * FLASH	64KiB
  * SRAM	20KiB
* Oscillators
  * HSI	8MHz
  * LSI	40kHz
  * HSE	8MHz
  * LSE	32.768kHz
* Power
  * Sources	
    * Any +3.3V pin (+3.3V)
    * Any +5V pin (+5V)
    * USB connector (+5V)
  * VDDA pin	No
  * VSSA pin	No
  * VREF- pin	No
  * VREF+ pin	No
  * Backup battery	None
  * Regulator
* Manufacturer	Shanghai TX Electronics Sci-Tech Co., Ltd
  * Part	TX6211B (DE=A1D)
  * Package	SOT23-5 5 pins
  * Input	+3.6V to +5.5V
  * Output	+3.3V @ 300mA
  * Datasheet	TX6211B.pdf
* PCB
  * Color	Blue
  * Size (w x l)	23mm x 53mm
  * Mounting	Breadboard


## STM32F103C8T6 Black Pill

Como en el caso anterior, su nombre proviene de la característica placa color negro, y lleva un procesador ARM Cortex M3 a 72 MHz.

![](imagenes/STM32F103C8T6_Black_Pill-1.jpg)
Fuente: [link](https://stm32-base.org/boards/STM32F103C8T6-Black-Pill)

Especificaciones Técnicas:

* Board
  * Name	Black Pill
  * Part	Unknown
  * Brand	Unknown
  * Origin	China
* Microcontroller
  * Part	STM32F103C8T6
  * Manufacturer	ST-Microelectronics
  * Core	Arm Cortex-M3
  * Max. Clock Speed	72MHz
  * Package	LQFP 48 pins
* Internal memories
  * FLASH	64KiB
  * SRAM	20KiB
* Oscillators
  * HSI	8MHz
  * LSI	40kHz
  * HSE	8MHz
  * LSE	32.768kHz
* Power
  * Sources	
    * Any +3.3V pin (+3.3V)
    * Any +5V pin (+5V)
    * USB connector (+5V)
  * VDDA pin	No
  * VSSA pin	No
  * VREF- pin	No
  * VREF+ pin	No
  * Backup battery	None
  * Regulator
* Manufacturer	Nanjing Micro One Electronics Inc.
  * Part	ME6211 (S2QA / S2QC / S2QK / S2RD)
  * Package	SOT23-5 5 pins
  * Input	+2V to +6V
  * Output	+3.3V @ 180mA
  * Datasheet	ME6211.pdf
* PCB
  * Color	Black
  * Size (w x l)	25mm x 57mm
  * Mounting	Breadboard


## El Proyecto STM32-base

Un sitio imprescindible de referencia para trabajar con estas placas es el [Proyecto STM32-base](https://stm32-base.org/) que provee valiosa información y se actualiza gracias a la participación activa de su comunidad de desarrolladores. Está pensado fundamentalmente para hobbystas y estudiantes.
